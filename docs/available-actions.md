# List of available actions

Yii2-user includes a lot of actions, which you can access by creating URLs for them. Here is the table of available
actions which contains route and short description of each action. You can create URLs for them using special Yii
helper `\yii\helpers\Url::to()`.

- **registration/register** Displays registration form
- **registration/resend**   Displays resend form
- **registration/confirm**  Confirms a user (requires *id* and *token* query params)
- **security/login**        Displays login form
- **security/logout**       Logs the user out (available only via POST method)
- **recovery/request**      Displays recovery request form
- **recovery/reset**        Displays password reset form (requires *id* and *token* query params)
- **settings/profile**      Displays profile settings form
- **settings/account**      Displays account settings form (email, username, password)
- **settings/networks**     Displays social network accounts settings page
- **profile/show**          Displays user's profile (requires *id* query param)
- **admin/index**           Displays user management interface

## Example of menu

You can add links to registration, login and logout as follows:

```php
Yii::$app->user->isGuest ?
    ['label' => 'Sign in', 'url' => ['security/login']] :
    ['label' => 'Sign out (' . Yii::$app->user->identity->username . ')',
        'url' => ['security/logout'],
        'linkOptions' => ['data-method' => 'post']],
['label' => 'Register', 'url' => ['registration/register'], 'visible' => Yii::$app->user->isGuest]
```