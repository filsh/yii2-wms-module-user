<?php

namespace tests\codeception\_fixtures;

use yii\test\ActiveFixture;

class UserFixture extends ActiveFixture
{
    public $modelClass = 'wms\user\models\User';
}
