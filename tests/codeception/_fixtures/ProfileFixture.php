<?php

namespace tests\codeception\_fixtures;

use yii\test\ActiveFixture;

class ProfileFixture extends ActiveFixture
{
    public $modelClass = 'wms\user\models\Profile';
}
