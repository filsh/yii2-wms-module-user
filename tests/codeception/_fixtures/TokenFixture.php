<?php

namespace tests\codeception\_fixtures;

use yii\test\ActiveFixture;

class TokenFixture extends ActiveFixture
{
    public $modelClass = 'wms\user\models\Token';
}
