<?php

if (Yii::$app->user->getIsGuest()) {
    echo \yii\helpers\Html::a('Login', ['security/login']);
    echo \yii\helpers\Html::a('Registration', ['registration/register']);
} else {
    echo \yii\helpers\Html::a('Logout', ['security/logout']);
}

echo $content;
