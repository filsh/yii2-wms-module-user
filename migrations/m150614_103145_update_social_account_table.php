<?php

use wms\user\models\Account;
use yii\db\Query;
use yii\db\Schema;
use yii\db\Migration;

class m150614_103145_update_social_account_table extends Migration
{
    public function up()
    {
        $this->addColumn(Account::tableName(), 'code', Schema::TYPE_STRING . '(32)');
        $this->addColumn(Account::tableName(), 'created_at', Schema::TYPE_INTEGER);
        $this->addColumn(Account::tableName(), 'email', Schema::TYPE_STRING);
        $this->addColumn(Account::tableName(), 'username', Schema::TYPE_STRING);
        $this->createIndex('account_unique_code', Account::tableName(), 'code', true);

        $accounts = (new Query())->from(Account::tableName())->select('id')->all();

        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($accounts as $account) {
                Yii::$app->db->createCommand()->update(Account::tableName(), [
                    'created_at' => time(),
                ], 'id = ' . $account['id'])->execute();
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public function down()
    {
        $this->dropIndex('account_unique_code', Account::tableName());
        $this->dropColumn(Account::tableName(), 'email');
        $this->dropColumn(Account::tableName(), 'username');
        $this->dropColumn(Account::tableName(), 'code');
        $this->dropColumn(Account::tableName(), 'created_at');
    }
}
