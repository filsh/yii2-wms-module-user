<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use wms\user\models\User;
use wms\user\models\Account;
use wms\user\migrations\Migration;
use yii\db\Schema;

/**
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class m140504_113157_update_tables extends Migration
{
    public function up()
    {
        // user table
        $this->dropIndex('user_confirmation', User::tableName());
        $this->dropIndex('user_recovery', User::tableName());
        $this->dropColumn(User::tableName(), 'confirmation_token');
        $this->dropColumn(User::tableName(), 'confirmation_sent_at');
        $this->dropColumn(User::tableName(), 'recovery_token');
        $this->dropColumn(User::tableName(), 'recovery_sent_at');
        $this->dropColumn(User::tableName(), 'logged_in_from');
        $this->dropColumn(User::tableName(), 'logged_in_at');
        $this->renameColumn(User::tableName(), 'registered_from', 'registration_ip');
        $this->addColumn(User::tableName(), 'flags', Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0');

        // account table
        $this->renameColumn(Account::tableName(), 'properties', 'data');
    }

    public function down()
    {
        // account table
        $this->renameColumn(Account::tableName(), 'data', 'properties');

        // user table
        $this->dropColumn(User::tableName(), 'flags');
        $this->renameColumn(User::tableName(), 'registration_ip', 'registered_from');
        $this->addColumn(User::tableName(), 'logged_in_at', Schema::TYPE_INTEGER);
        $this->addColumn(User::tableName(), 'logged_in_from', Schema::TYPE_INTEGER);
        $this->addColumn(User::tableName(), 'recovery_sent_at', Schema::TYPE_INTEGER);
        $this->addColumn(User::tableName(), 'recovery_token', Schema::TYPE_STRING . '(32)');
        $this->addColumn(User::tableName(), 'confirmation_sent_at', Schema::TYPE_INTEGER);
        $this->addColumn(User::tableName(), 'confirmation_token', Schema::TYPE_STRING . '(32)');
        $this->createIndex('user_confirmation', User::tableName(), 'id, confirmation_token', true);
        $this->createIndex('user_recovery', User::tableName(), 'id, recovery_token', true);
    }
}
