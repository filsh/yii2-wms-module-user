<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use wms\user\Module;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var wms\user\models\User $model
 * @var wms\user\models\Account $account
 */

$this->title = Module::t('user', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-box">
    <div class="login-logo">
        <?= Yii::$app->name ?>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg"><?= Html::encode($this->title) ?></p>
        <div class="alert alert-info">
            <p>
                <?= Module::t('user', 'In order to finish your registration, we need you to enter following fields') ?>:
            </p>
        </div>
        <?php $form = ActiveForm::begin([
            'id' => 'connect-account-form',
        ]); ?>

        <?= $form->field($model, 'email') ?>

        <?= $form->field($model, 'username') ?>

        <?= Html::submitButton(Module::t('user', 'Continue'), ['class' => 'btn btn-success btn-block']) ?>

        <?php ActiveForm::end(); ?>
        <p class="text-center">
            <?= Html::a(Module::t('user', 'If you already registered, sign in and connect this account on settings page'), ['settings/networks']) ?>.
        </p>
    </div>
</div>
