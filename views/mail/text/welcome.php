<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use wms\user\Module;

/**
 * @var wms\user\models\User
 */
?>
<?= Module::t('user', 'Hello') ?>,

<?= Module::t('user', 'Your account on {0} has been created', Yii::$app->name) ?>.
<?php if ($module->enableGeneratingPassword): ?>
<?= Module::t('user', 'We have generated a password for you') ?>:
<?= $user->password ?>
<?php endif ?>

<?php if ($token !== null): ?>
<?= Module::t('user', 'In order to complete your registration, please click the link below') ?>.

<?= $token->url ?>

<?= Module::t('user', 'If you cannot click the link, please try pasting the text into your browser') ?>.
<?php endif ?>

<?= Module::t('user', 'If you did not make this request you can ignore this email') ?>.
