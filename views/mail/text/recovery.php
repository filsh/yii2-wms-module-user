<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use wms\user\Module;
/**
 * @var wms\user\models\User   $user
 * @var wms\user\models\Token  $token
 */
?>
<?= Module::t('user', 'Hello') ?>,

<?= Module::t('user', 'We have received a request to reset the password for your account on {0}', Yii::$app->name) ?>.
<?= Module::t('user', 'Please click the link below to complete your password reset') ?>.

<?= $token->url ?>

<?= Module::t('user', 'If you cannot click the link, please try pasting the text into your browser') ?>.

<?= Module::t('user', 'If you did not make this request you can ignore this email') ?>.
