<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use wms\user\models\User;
use yii\bootstrap\Nav;
use yii\web\View;
use wms\user\Module;

/**
 * @var View 	$this
 * @var User 	$user
 * @var string 	$content
 */

$this->title = Module::t('user', 'Update user account');
$this->params['breadcrumbs'][] = ['label' => Module::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<?= $this->render('/_alert', [
    'module' => Yii::$container->get(Module::class),
]) ?>

<?= $this->render('_menu') ?>

<div class="row">
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-body">
                <?= Nav::widget([
                    'options' => [
                        'class' => 'nav-pills nav-stacked',
                    ],
                    'items' => [
                        ['label' => Module::t('user', 'Account details'), 'url' => ['admin/update', 'id' => $user->id]],
                        ['label' => Module::t('user', 'Profile details'), 'url' => ['admin/update-profile', 'id' => $user->id]],
                        ['label' => Module::t('user', 'Information'), 'url' => ['admin/info', 'id' => $user->id]],
                        [
                            'label' => Module::t('user', 'Assignments'),
                            'url' => ['admin/assignments', 'id' => $user->id],
                            'visible' => isset(Yii::$app->extensions['dektrium/yii2-rbac']),
                        ],
                        '<hr>',
                        [
                            'label' => Module::t('user', 'Confirm'),
                            'url'   => ['admin/confirm', 'id' => $user->id],
                            'visible' => !$user->isConfirmed,
                            'linkOptions' => [
                                'class' => 'text-success',
                                'data-method' => 'post',
                                'data-confirm' => Module::t('user', 'Are you sure you want to confirm this user?'),
                            ],
                        ],
                        [
                            'label' => Module::t('user', 'Block'),
                            'url'   => ['admin/block', 'id' => $user->id],
                            'visible' => !$user->isBlocked,
                            'linkOptions' => [
                                'class' => 'text-danger',
                                'data-method' => 'post',
                                'data-confirm' => Module::t('user', 'Are you sure you want to block this user?'),
                            ],
                        ],
                        [
                            'label' => Module::t('user', 'Unblock'),
                            'url'   => ['admin/block', 'id' => $user->id],
                            'visible' => $user->isBlocked,
                            'linkOptions' => [
                                'class' => 'text-success',
                                'data-method' => 'post',
                                'data-confirm' => Module::t('user', 'Are you sure you want to unblock this user?'),
                            ],
                        ],
                        [
                            'label' => Module::t('user', 'Delete'),
                            'url'   => ['admin/delete', 'id' => $user->id],
                            'linkOptions' => [
                                'class' => 'text-danger',
                                'data-method' => 'post',
                                'data-confirm' => Module::t('user', 'Are you sure you want to delete this user?'),
                            ],
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-body">
                <?= $content ?>
            </div>
        </div>
    </div>
</div>
