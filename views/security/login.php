<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wms\user\Module;
use wms\user\widgets\Connect;

/**
 * @var yii\web\View                   $this
 * @var wms\user\models\LoginForm $model
 * @var wms\user\Module           $module
 */
$this->title = Module::t('user', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('/_alert', ['module' => $module]) ?>

<div class="login-box">
    <div class="login-logo">
        <?= Yii::$app->name ?>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg"><?= Html::encode($this->title) ?></p>
        <?php $form = ActiveForm::begin([
            'id'                     => 'login-form',
            'enableAjaxValidation'   => true,
            'enableClientValidation' => false,
            'validateOnBlur'         => false,
            'validateOnType'         => false,
            'validateOnChange'       => false,
        ]) ?>

        <?= $form->field($model, 'login', ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']]) ?>

        <?= $form->field($model, 'password', ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2']])->passwordInput()->label(Module::t('user', 'Password') . ($module->enablePasswordRecovery ? ' (' . Html::a(Module::t('user', 'Forgot password?'), ['recovery/request'], ['tabindex' => '5']) . ')' : '')) ?>

        <?= $form->field($model, 'rememberMe')->checkbox(['tabindex' => '4']) ?>

        <?= Html::submitButton(Module::t('user', 'Sign in'), ['class' => 'btn btn-primary btn-block', 'tabindex' => '3']) ?>

        <?php ActiveForm::end(); ?>
        <?php if ($module->enableConfirmation): ?>
            <p class="text-center">
                <?= Html::a(Module::t('user', 'Didn\'t receive confirmation message?'), ['registration/resend']) ?>
            </p>
        <?php endif ?>
        <?php if ($module->enableRegistration): ?>
            <p class="text-center">
                <?= Html::a(Module::t('user', 'Don\'t have an account? Sign up!'), ['registration/register']) ?>
            </p>
        <?php endif ?>
        <?= Connect::widget([
            'baseAuthUrl' => ['security/auth'],
        ]) ?>
    </div>
</div>